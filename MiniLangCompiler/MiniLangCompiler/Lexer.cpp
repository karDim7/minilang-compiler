#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include "Lexer.h"
using namespace std;


	Tokens Lexer:: checkResWords(string lexeme) {
		if (!lexeme.compare("int") || !lexeme.compare("real") || !lexeme.compare("bool") || !lexeme.compare("string"))
		{
			return TOK_Type;
		}
		else if (!lexeme.compare("if"))
		{
			return TOK_IfStmt;
		}
		else if (!lexeme.compare("var"))
		{
			return TOK_Var;
		}
		else if (!lexeme.compare("def"))
		{
			return TOK_Def;
		}
		else if (!lexeme.compare("true") || !lexeme.compare("false"))
		{
			return TOK_BoolLiteral;
		}
		else if (!lexeme.compare("while"))
		{
			return TOK_WhileStmt;
		}
		else if (!lexeme.compare("print"))
		{
			return TOK_Print;
		}
		else if (!lexeme.compare("set"))
		{
			return TOK_Set;
		}
		else if (!lexeme.compare("for"))
		{
			return TOK_For;
		}
		else if (!lexeme.compare("else"))
		{
			return TOK_Else;
		}
		else if (!lexeme.compare("return"))
		{
			return TOK_Return;
		}
		return TOK_String;
	}

	vector<Tokens> Lexer::CharStream(vector<Tokens> v)
	{
	
		vector<string> words;
		ifstream input("history.txt");
		char ch = NULL;
		char nextChar;
		string check = "";
		string lexeme = "";
		Tokens t;
		Tokens temp;
		Token tok;
		int count = 0;

		bool openString = false;
		bool stringLiteral = false;
		bool comment = false;

		while (!input.eof())
		{
			if (ch == NULL)
			{
				input.get(ch);
			}

			input.get(nextChar);
			check += ch;
			temp = tok.TokReturn(check);
			check = "";


			if (ch == '/' && nextChar == '/')
			{
				comment = true;
			}

			// when '"' is first recieved it is marked as an open string literal and then closed when a '"' is received again.;
			if (ch == '"') {
				if (openString == false)
				{
					openString = true;
					stringLiteral = true;

				}
				else {
					openString = false;
				}
			}

			//whatever is written after the comment markings '//' is accepted until it gets to the next line
			if (comment == true && ch != '\n')
			{
				lexeme += ch;
			}
			//once the next line is reached, the lexeme is closed and comment token returned
			else if (comment == true && ch == '\n')
			{
				t = TOK_CMT;
				comment = false;
				
				words.push_back(lexeme);
				lexeme = "";
				v.push_back(t);
				
			}
			// if the string literal is open it accepts everything 
			else if (openString == true || ch == '"')
			{
				lexeme += ch;

			}
			// if a string literal has been generated and close it will return the token (provided that the lexeme is not blank)
			else if (openString == false && lexeme != "" && stringLiteral == true)
			{
				t = tok.TokReturn(lexeme);
				stringLiteral = false;
				words.push_back(lexeme);
				lexeme = "";
				v.push_back(t);
				
				//cout << t << " ";
			}
			//as long as the character provided is not a space, an operator or brackets it will append to the lexeme
			else if (!isspace(ch) && temp != TOK_MultOpetator && temp != TOK_Operator && temp != TOK_RelOperator && temp != TOK_EndLine && temp != TOK_OBrace && temp != TOK_OCurlyBrace && temp != TOK_CCurlyBrace && temp != TOK_CBrace&& temp != TOK_Col && temp != TOK_Comma && ch != '!')
			{
				lexeme += ch;

			}
			//if the lexeme is closed and not an empty string it will return the token
			else if (lexeme != "" && temp != TOK_RelOperator && ch != '!'&& stringLiteral != true ) {
				t = tok.TokReturn(lexeme);
				// if the lexeme is a string it cheks if it is a reserved word
				if (t == TOK_String)
				{
					t = checkResWords(lexeme);
				}
				words.push_back(lexeme);
				lexeme = "";
				v.push_back(t);
				//cout << t<<" ";
			}
			// if the charater is an operator or a ';' it will return the token provided that ch is not a space or a coomment marking
			if (!isspace(ch) && (temp == TOK_MultOpetator || temp == TOK_Operator || temp == TOK_RelOperator || temp == TOK_EndLine || temp == TOK_OBrace || temp == TOK_OCurlyBrace || temp == TOK_CCurlyBrace || temp == TOK_CBrace || temp == TOK_Col || temp == TOK_Comma || ch == '!') && comment == false && stringLiteral != true) {

				if ((temp == TOK_RelOperator || ch == '!') && nextChar == '='&& count<1)
				{
					lexeme += ch;
					count++;

				}
				else {
					count = 0;
					lexeme += ch;
					t = tok.TokReturn(lexeme);
					v.push_back(t);
					words.push_back(lexeme);
					lexeme = "";
				}
			}


			ch = nextChar;
		}

		lexemes = words;
		
		return v;

	};

	vector<string> Lexer::returnWords(vector<string> w)
	{
		return w;
	}

	Tokens Lexer::GetNextToken(int i) {
		vector<Tokens> TokenStream;
		
		TokenStream = CharStream(TokenStream);
		
		return TokenStream[i];
	}
	string Lexer::GetCode(int i) {
		vector<string> words;
		words = this->lexemes;
		string lexeme = words[i];
		return lexeme;
	}

