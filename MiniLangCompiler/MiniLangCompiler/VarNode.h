#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Node.h"


using namespace std;

class VarNode :public Node
{
public:
	Lexer lex;
	int count;

	VarNode(int counter)
	{
		this->count = counter;
		this->Value = TOK_Var;
		count++;
	}

	VarNode ParseVarDecl();
};