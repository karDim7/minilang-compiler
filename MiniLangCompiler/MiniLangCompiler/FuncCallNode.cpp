#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include "FuncCallNode.h"
#include "FormalParamNode.h"

using namespace std;
FuncCallNode FuncCallNode::ParseFuncCallStmt() {

	Tokens next_tok;
	int ind = 0;
	string lexeme;
	do {
		Node n;
		next_tok = lex.GetNextToken(count);
		lexeme = lex.GetCode(count);
		switch (ind) {

		case 0:
			if ( next_tok == TOK_Identifier || next_tok == TOK_String )
			{
				this->Value = next_tok;
				this->code = lexeme;
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "identifier expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 1:
			if (next_tok == TOK_OBrace)
			{
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "'('  expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 2:
			if (next_tok == TOK_Int || next_tok == TOK_Decimal || next_tok == TOK_Identifier || next_tok == TOK_String || next_tok == TOK_BoolLiteral || next_tok == TOK_StringLiteral)
			{
				FormalParamNode fpn(count);
				count = fpn.count;
				this->appendNode(fpn);
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else if (next_tok == TOK_CBrace)
			{
				ind++;
			}
			else {
				cout << "parameter  expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 3:
			if (next_tok == TOK_CBrace)
			{
			
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "')'  expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 4:
			if (next_tok == TOK_EndLine)
			{
				
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "';'  expected" << endl;
				getchar();
				exit(0);
			}
			break;
		}
	} while (next_tok != TOK_EndLine);

	return *this;
}