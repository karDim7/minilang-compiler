#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include "WhileNode.h"
#include "ExpressionNode.h"
#include "BlockNode.h"
using namespace std;

WhileNode WhileNode::ParseWhileStmt()
{
	// to check if Stmt

	Tokens next_tok;
	int ind = 0;
	string lexeme;
	do {
		Node n;
		next_tok = lex.GetNextToken(count);

		lexeme = lex.GetCode(count);
		switch (ind) {
		case 0:
			if (next_tok == TOK_OBrace)
			{
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "'(' expected" << endl;
				getchar();
				exit(0);
			}
			break;

		case 1:
			if (next_tok == TOK_Int || next_tok == TOK_Decimal || next_tok == TOK_Identifier || next_tok == TOK_String || next_tok == TOK_BoolLiteral || next_tok == TOK_StringLiteral)
			{
				ExpressionNode en(count);
				count = en.count;
				this->appendNode(en);
				ind++;
			}
			else if (next_tok == TOK_CMT) {

			}
			else {
				cout << "an expression expected" << endl;
			}
			break;
		case 2:
			if (next_tok == TOK_CBrace)
			{
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "')' expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 3:
			if (next_tok == TOK_OCurlyBrace)
			{
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "'{' expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 4:
			if (next_tok == TOK_Def || next_tok == TOK_Var || next_tok == TOK_IfStmt || next_tok == TOK_WhileStmt || next_tok == TOK_Identifier || next_tok == TOK_Print)
			{
				vector<Node> v;
				BlockNode bn(count);
				bn.ParseBlock();
				this->appendNode(bn);
				count = bn.count;
				ind++;
			}
			else if (next_tok == TOK_CMT || next_tok == TOK_CCurlyBrace) {
				count++;
			}
			else {
				cout << "block expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 5:
			if (next_tok == TOK_CCurlyBrace)
			{
				ind++;

			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "'}' expected" << endl;
				getchar();
				//exit(0);
			}
			break;


		}

	} while (next_tok != TOK_CCurlyBrace);

	return *this;
}
