#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Node.h"


using namespace std;

class IfNode :public Node
{
public:
	Lexer lex;
	int count;

	IfNode(int counter)
	{
		this->count = counter;
		this->Value = TOK_IfStmt;
		count++;
	}

	IfNode ParseIfStmt();
};