#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Node.h"


using namespace std;

class AssignmentNode :public Node
{
public:
	Lexer lex;
	int count;

	AssignmentNode(int counter)
	{
		this->count = counter;
		this->Value = TOK_Set;
		count++;
	}

	AssignmentNode ParseAssignmentStmt();
};