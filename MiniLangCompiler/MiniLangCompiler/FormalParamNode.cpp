#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include "FormalParamNode.h"

using namespace std;
FormalParamNode FormalParamNode::ParseFormalParamStmt() {

	Tokens next_tok;
	int ind = 0;
	string lexeme;
	do {
		Node n;
		next_tok = lex.GetNextToken(count);
		lexeme = lex.GetCode(count);
		switch (ind) {

		case 0:
			if (next_tok == TOK_Identifier || next_tok == TOK_String )
			{
				n.Value = next_tok;
				this->appendNode(n);
				n.code = lexeme;
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "identifier expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 1:
			if (next_tok == TOK_Col)
			{
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "':'  expected" << endl;
				
				getchar();
				exit(0);
			}
			break;
		case 2:
			if (next_tok == TOK_Type)
			{
				n.Value = next_tok;
				n.code = lexeme;
				this->appendNode(n);
				
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "type expected" << endl;
				getchar();
				exit(0);
			}
			break;
		
		}
	} while (ind<3);

	return *this;
}
