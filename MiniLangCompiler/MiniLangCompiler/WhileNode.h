#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Node.h"


using namespace std;

class WhileNode :public Node
{
public:
	Lexer lex;
	int count;

	WhileNode(int counter)
	{
		this->count = counter;
		this->Value = TOK_WhileStmt;
		count++;
	}

	WhileNode ParseWhileStmt();
};