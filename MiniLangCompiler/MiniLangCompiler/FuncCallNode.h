#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Node.h"


using namespace std;

class FuncCallNode :public Node
{
public:
	Lexer lex;
	int count;

	FuncCallNode(int counter)
	{
		this->count = counter;
				
	}

	FuncCallNode ParseFuncCallStmt();
};