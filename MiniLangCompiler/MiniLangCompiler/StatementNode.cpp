#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include "StatementNode.h"
#include "VarNode.h"
#include "AssignmentNode.h"
#include "IfNode.h"
#include "WhileNode.h"
#include "ReturnNode.h"
#include "FuncNode.h"
#include "PrintNode.h"
#include "FuncCallNode.h"


using namespace std;
Node StatementNode::ParseStatement() {
	Tokens next_tok;
		Node n;
		VarNode vn = VarNode(count);
		AssignmentNode an = AssignmentNode(count);
		IfNode in = IfNode(count);
		WhileNode wn = WhileNode(count);
		ReturnNode rn = ReturnNode(count);
		FuncNode fn = FuncNode(count);
		FuncCallNode fcn = FuncCallNode(count);
		PrintNode pn = PrintNode(count);

		next_tok = lex.GetNextToken(count);
		switch (next_tok)
		{
		case TOK_Var:
			
			vn = vn.ParseVarDecl();
			n = vn;
			count = vn.count;
			this->Value = vn.Value;
			this->children = vn.children;
			this->code = vn.code;
			return *this;
			break;
		case TOK_Set:			
			an = an.ParseAssignmentStmt();
			n = an;
			count = an.count;
			this->Value = an.Value;
			this->children = an.children;
			this->code = an.code;
			return *this;
			break;
		case TOK_IfStmt:
			in = in.ParseIfStmt();
			n = in;
			this->count = in.count;
			this->Value = in.Value;
			this->children = in.children;
			this->code = in.code;
			return *this;
			break;
		case TOK_WhileStmt:
			wn = wn.ParseWhileStmt();
			n = wn;
			this->count = wn.count;
			this->Value = wn.Value;
			this->children = wn.children;
			this->code = wn.code;
			return *this;
			break;
		case TOK_Return:
			rn = rn.ParseReturnStmt();
			n = rn;
			count = rn.count;
			this->Value = rn.Value;
			this->children = rn.children;
			this->code = rn.code;
			return *this;
			break;
		case TOK_Def:
			fn = fn.ParseFuncStmt();
			n = fn;
			count = fn.count;
			this->Value = fn.Value;
			this->children = fn.children;
			this->code = fn.code;
			return *this;
			break;
		case TOK_String:
			fcn = fcn.ParseFuncCallStmt();
			n = fcn;
			count = fcn.count;
			this->Value = fcn.Value;
			this->children = fcn.children;
			this->code = fcn.code;
			return *this;
			break;
		case TOK_Identifier:
			fcn = fcn.ParseFuncCallStmt();
			n = fcn;
			count = fcn.count;
			this->Value = fcn.Value;
			this->children = fcn.children;
			this->code = fcn.code;
			return *this;
			break;
		case TOK_Print:
			pn = pn.ParsePrintStmt();
			n = pn;
			count = pn.count;
			this->Value = pn.Value;
			this->children = pn.children;
			this->code = pn.code;
			return *this;
			break;
		}
		return *this;
}
