#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Node.h"


using namespace std;

class ReturnNode :public Node
{
public:
	Lexer lex;
	int count;

	ReturnNode(int counter)
	{
		this->count = counter;
		this->Value = TOK_Return;
		count++;
	}

	ReturnNode ParseReturnStmt();
};