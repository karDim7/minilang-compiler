#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Lexer.h"
#include "Node.h"
#include <stack>  

using namespace std;

class Variable {
public:
	Tokens type;
	int scope;
	string name;
	string value;

	Variable(Tokens varType, int varScope, string varName, string varValue)
	{
		type = varType;
		scope = varScope;
		name = varName;
		value = varValue;
	}

};
