#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include "ExpressionNode.h"


using namespace std;
Node ExpressionNode::ParseExpression() {
	Tokens next_tok, tok_next1;
	Node tempNode;
	string lexeme;
	

	int ind = 0;

	do{
		Node n;

		/*ExpressionNode en(count);*/
		next_tok = lex.GetNextToken(count);
		if (count < lex.lexemes.size())
		{
			tok_next1 = lex.GetNextToken(count + 1);
		}
		else
		{
			tok_next1 = TOK_EOF;
		}

		lexeme = lex.GetCode(count);

		switch (ind) {

		case 0:
			if (next_tok == TOK_Int || next_tok == TOK_Decimal || next_tok == TOK_Identifier || next_tok == TOK_String)
			{
				tempNode.Value = next_tok;
				tempNode.code = lexeme;
				count++;				
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "value expected" << endl;
				getchar();
				exit(0);
			}
			break; 
		case 1:
			if (next_tok == TOK_RelOperator || next_tok == TOK_Operator || next_tok == TOK_MultOpetator)
			{
				this->Value = next_tok;
				this->code = lexeme;
				this->appendNode(tempNode);
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "operator expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 2:
			
		   if (tok_next1 == TOK_RelOperator || tok_next1 == TOK_Operator || tok_next1 == TOK_MultOpetator)
			{
			   /*cout << lexeme << endl;*/
			   ind++;
			   ExpressionNode en(count);
				n = en.ParseExpression();	
				this->appendNode(n);
				count = en.count;
				
				
			}
			else if (next_tok == TOK_Int || next_tok == TOK_Decimal || next_tok == TOK_Identifier || next_tok == TOK_String)
			{
				n.Value = next_tok;
				n.code = lexeme;
				count++;				
				this->appendNode(n);
			    ind++;
				return *this;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "value expected" << endl;
				getchar();
				exit(0);
			}
			break;
		}	


	} while (ind <3);
	return *this;
}