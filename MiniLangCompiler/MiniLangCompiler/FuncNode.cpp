#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include "FuncNode.h"
#include "FormalParamNode.h"
#include "BlockNode.h"

using namespace std;
FuncNode FuncNode::ParseFuncStmt() {

	Tokens next_tok;
	int ind = 0;
	do {
		Node n;
		next_tok = lex.GetNextToken(count);
		switch (ind) {

		case 0:
			if ( next_tok == TOK_Identifier || next_tok == TOK_String )
			{
				n.Value = next_tok;
				this->appendNode(n);
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "identifier expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 1:
			if (next_tok == TOK_OBrace)
			{
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "'('  expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 2:
			if (next_tok == TOK_Identifier || next_tok == TOK_String)
			{
				FormalParamNode fpn = FormalParamNode(count).ParseFormalParamStmt() ; 
				n = fpn;
				this->appendNode(n);
				count = fpn.count;
				ind++;
			}
			else if (next_tok == TOK_CBrace)
			{
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "parameter  expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 3:
			if (next_tok == TOK_CBrace)
			{
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "')'  expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 4:
			if (next_tok == TOK_Col)
			{
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "':'  expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 5:
			if (next_tok == TOK_Type)
			{
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "type  expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 6:
			if (next_tok == TOK_OCurlyBrace)
			{
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "'{'  expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 7:
			if (next_tok == TOK_Int || next_tok == TOK_Decimal || next_tok == TOK_Identifier || next_tok == TOK_String || next_tok == TOK_BoolLiteral || next_tok == TOK_StringLiteral || next_tok == TOK_IfStmt || next_tok == TOK_WhileStmt)
			{
				BlockNode bn(count);
				bn.ParseBlock();
				this->appendNode(bn);
				count = bn.count;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "block  expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 8:
			if (next_tok == TOK_CCurlyBrace)
			{
				
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "'{'  expected" << endl;
				getchar();
				exit(0);
			}
			break;
		}
	} while (ind < 9);

	return *this;
}