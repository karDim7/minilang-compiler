#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Node.h"


using namespace std;

class ExpressionNode :public Node
{
public:
	Lexer lex;
	int count;

	ExpressionNode(int counter)
	{
		this->count = counter;
		this ->ParseExpression();
		
	}

	Node ParseExpression();
};