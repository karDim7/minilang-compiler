#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include "StatementNode.h"
#include "BlockNode.h"

using namespace std;
Node BlockNode::ParseBlock() {
	
	Tokens next_tok;
	Node n;
	do {
		next_tok = lex.GetNextToken(count);
		if (next_tok == TOK_CMT)
		{
					count++;
		}
		else if (next_tok != TOK_CCurlyBrace)
		{
			StatementNode sn(count);
			sn.ParseStatement();
			this->Value = sn.Value;
			this->appendNode(sn);
			nodes.push_back(sn);
			count = sn.count;
		}
		
	} while (next_tok != TOK_CCurlyBrace);

	if (count < lex.lexemes.size() - 1)
	{
		count++;
	}
	return *this;
}