#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include "AssignmentNode.h"
#include "ExpressionNode.h"
using namespace std;


AssignmentNode AssignmentNode:: ParseAssignmentStmt() {

	Tokens next_tok;
	int ind = 0;
	do {
		Node n;

		next_tok = lex.GetNextToken(count);
		switch (ind) {
		case 0:
			if (next_tok == TOK_Identifier || next_tok == TOK_String)
			{
				n.Value = next_tok;
				this->appendNode(n);
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "identifier name expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 1:
			if (next_tok == TOK_RelOperator)
			{
				n.Value = next_tok;
				this->appendNode(n);
				count++;
				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "'='  expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 2:
			if (next_tok == TOK_Int || next_tok == TOK_Decimal || next_tok == TOK_Identifier || next_tok == TOK_String || next_tok == TOK_BoolLiteral || next_tok == TOK_StringLiteral)
			{
				
				ExpressionNode en(count);
				count = en.count;

				ind++;
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "value expected" << endl;
				getchar();
				exit(0);
			}
			break;
		case 3:
			if (next_tok == TOK_EndLine)
			{

				n.Value = next_tok;
				this->appendNode(n);
				count++;
				
			}
			else if (next_tok == TOK_CMT) {
				count++;
			}
			else {
				cout << "';'  expected" << endl;
				getchar();
				exit(0);
			}
			break;

		}
	} while (next_tok != TOK_EndLine);

	return *this;
}