#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Node.h"

using namespace std;

class BlockNode :public Node
{
public:
	Lexer lex;
	int count;
	vector<Node> nodes;
	BlockNode(int counter)
	{
		this->count = counter;
		this->ParseBlock();
		
		
	}

	Node ParseBlock();
};
