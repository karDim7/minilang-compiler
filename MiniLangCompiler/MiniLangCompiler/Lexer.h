#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <stdio.h>
#include <stdlib.h>

using namespace std;


enum Tokens {
	TOK_Identifier = 2,
	TOK_String = 1,
	TOK_Int = 3,
	TOK_Decimal = 5,
	TOK_StringLiteral = 8,
	TOK_Operator = 9,
	TOK_RelOperator = 11,
	TOK_MultOpetator = 13,
	TOK_EndLine = 14,
	TOK_OCurlyBrace = 15,
	TOK_CCurlyBrace = 16,
	TOK_OBrace = 17,
	TOK_CBrace = 18,
	TOK_IfStmt = 19,
	TOK_WhileStmt = 20,
	TOK_For = 21,
	TOK_Type = 22,
	TOK_BoolLiteral = 23,
	TOK_Set = 24,
	TOK_Var = 25,
	TOK_Print = 26,
	TOK_Def = 27,
	TOK_CMT = 28,
	TOK_Col = 29,
	TOK_Comma = 30,
	TOK_Else = 32,
	TOK_Return = 31,
	ERROR = -1,
	TOK_EOF = 0
};

struct Token {

	Tokens TokReturn(string lexeme) {
		/*cout << lexeme;*/
		int States[17][21] = {
			{ 3,2,2,3,5,5,7,7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },	   // 0-9
			{ 1,1,2,-1,-1,-1,7,7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },   // a-z
			{ 2,2,2,-1,-1,-1,7,7,-1,-1,-1,-1,-1,-1,-1 ,-1,-1,-1,-1,-1,-1 },   // '_'
			{ -1,-1,-1,4,-1,-1,7,7,-1,-1,-1,-1,-1,-1,-1 ,-1,-1,-1,-1,-1,-1 }, // '.'
			{ 6,-1,-1,-1,-1,-1,8,8,-1,-1,-1,-1,-1,-1,-1 ,-1,-1,-1,-1,-1,-1 }, // '"'
			{ 9,-1,-1,-1,-1,-1,7,7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 }, // '+' and '-' and 'and'
			{ 10,-1,-1,-1,-1,-1,7,7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },// '>','<' and '='
			{ 12,-1,-1,-1,-1,-1,7,7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },// '!'
			{ 10,-1,-1,-1,-1,-1,7,7,-1,-1,11,-1,11,-1,-1,-1,-1,-1,-1,-1,-1 },//'='
			{ 13,-1,-1,-1,-1,-1,7,7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },// '*' and '/' and 'or'
			{ 14,-1,-1,-1,-1,-1,7,7,-1,-1,-1,-1,-1,-1,-1 ,-1,-1,-1,-1,-1,-1 },//';'
			{ 15,-1,-1,-1,-1,-1,7,7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },// '{'
			{ 16,-1,-1,-1,-1,-1,7,7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },// '}'
			{ 17,-1,-1,-1,-1,-1,7,7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },// ')'
			{ 18,-1,-1,-1,-1,-1,7,7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },// '('
			{ 19,-1,-1,-1,-1,-1,7,7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },// ':'
			{ 20,-1,-1,-1,-1,-1,7,7,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1 },// ','
		};
		int state = 0;
		for (int i = 0; i < lexeme.length(); i++)
		{
			char c = lexeme.at(i);
			if (isalpha(c))
			{
				state = States[1][state];
			}
			else if (isdigit(c)) {
				state = States[0][state];
			}
			else if (c == '_') {
				state = States[2][state];
			}
			else if (c == '.') {
				state = States[3][state];
			}
			else if (c == '"') {
				state = States[4][state];
			}
			else if (c == '+' || c == '-') {
				state = States[5][state];
			}
			else if (c == '=') {
				state = States[8][state];
			}
			else if (c == '>' || c == '<' || c == '=') {
				state = States[6][state];
			}
			else if (c == '!') {
				state = States[7][state];
			}
			else if (c == '*' || c == '/') {
				state = States[9][state];
			}
			else if (c == ';') {
				state = States[10][state];
			}
			else if (c == '{') {
				state = States[11][state];
			}
			else if (c == '}') {
				state = States[12][state];
			}
			else if (c == '(') {
				state = States[13][state];
			}
			else if (c == ')') {
				state = States[14][state];
			}
			else if (c == ':') {
				state = States[15][state];
			}
			else if (c == ',') {
				state = States[16][state];
			}

		}
		switch (state) {
		case 1: return TOK_String;
			break;
		case 2: return TOK_Identifier;
			break;
		case 3: return TOK_Int;
			break;
		case 4:return ERROR;
			break;
		case 5:return TOK_Decimal;
			break;
		case 6:return ERROR;
			break;
		case 7:return ERROR;
			break;
		case 8: return TOK_StringLiteral;
			break;
		case 9:return TOK_Operator;
			break;
		case 10: return TOK_RelOperator;
			break;
		case 11: return TOK_RelOperator;
			break;
		case 12: return ERROR;
			break;
		case 13:return TOK_MultOpetator;
			break;
		case 14: return TOK_EndLine;
			break;
		case 15: return TOK_OCurlyBrace;
			break;
		case 16: return TOK_CCurlyBrace;
			break;
		case 17: return TOK_OBrace;
			break;
		case 18: return TOK_CBrace;
			break;
		case 19: return TOK_Col;
			break;
		case 20: return TOK_Comma;
			break;
		case -1: return ERROR;
			break;
		}
		return ERROR;
	}
};
class Lexer
{
public:
	vector<string> lexemes;

	Tokens checkResWords(string lexeme);
	vector<Tokens> CharStream(vector<Tokens> v  );
	vector<string> returnWords(vector<string> w);
	Tokens GetNextToken(int i);
	string GetCode(int i);

};