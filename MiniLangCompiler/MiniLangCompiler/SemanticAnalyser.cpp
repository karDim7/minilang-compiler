#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "SemanticAnalyser.h"

stack<Variable> SemanticAnalyser::Traversal()
{
	Tokens type;
	int scope;
	string name;
	string value;

		
		scope = this->scope;
		for (int i = 0; i < node.children.size(); i++)
		{
			Node n = node.children[i];
			if (node.Value == TOK_Var)
			{
				
				if (i == 0)// to get the name of the variable
				{
					name = n.code;
				}
				else if (i == 1) // to get the type of the Variable
				{
					type = n.Value;
				}
				value = "";
				Variable v(type, scope, name, value);
				s.push(v);
			}
			else if (node.Value == TOK_Def)
			{
				SemanticAnalyser(n, scope+1, s);
			}
			else
			{
				SemanticAnalyser(n, scope, s);
			}
			
		}
		return s;
	}
