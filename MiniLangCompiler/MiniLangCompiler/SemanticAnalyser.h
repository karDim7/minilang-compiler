#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <stack>
#include "Variable.h"
using namespace std;
class SemanticAnalyser {
public:
	Node node;
	int scope;
	stack<Variable> s;
	SemanticAnalyser(Node n , int scope, stack<Variable> stack)
	{
		node = n;
		this->scope = scope;
		s = stack;
	}
	stack<Variable> Traversal();

};