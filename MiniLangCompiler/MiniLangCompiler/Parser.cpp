#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include "Parser.h"

using namespace std;

Node Parser::ParseCode()
{
	int count =0;
	Lexer lex;
	vector<Tokens> size;
	size = lex.CharStream(size);
	vector<Node> v;
	vector<vector<Node>> program;
	
	do {
		BlockNode bn(count);
        bn.ParseBlock();
		//v.push_back(bn);
		this->AST_Node.appendNode(bn);
		count = bn.count;
	} while (count < size.size() - 1);

	/*vector<Node> temp;
	vector<Node> children;
	for (int i = 0; i < program.size(); i++)
	{
		temp = program[i];
		for (int j = 0; j < temp.size(); j++)
		{
			children.push_back(temp[j]);
		}
	}*/
	return this->AST_Node;
}