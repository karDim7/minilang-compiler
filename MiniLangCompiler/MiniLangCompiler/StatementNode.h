#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Node.h"


using namespace std;

class StatementNode :public Node
{
public:
	Lexer lex;
	int count;

	StatementNode(int counter)
	{
		this->count = counter;
				
	}

	Node ParseStatement();
};