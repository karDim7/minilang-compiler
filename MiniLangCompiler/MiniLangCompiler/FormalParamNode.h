#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Node.h"


using namespace std;

class FormalParamNode :public Node
{
public:
	Lexer lex;
	int count;
	
	FormalParamNode(int counter)
	{
		this->count = counter;
		this->ParseFormalParamStmt();
		count++;
	}
	
	FormalParamNode ParseFormalParamStmt();
};